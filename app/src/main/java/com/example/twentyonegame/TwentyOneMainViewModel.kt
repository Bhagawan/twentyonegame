package com.example.twentyonegame

import android.os.Build
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.twentyonegame.data.AssetsState
import com.example.twentyonegame.data.objects.AppData
import com.example.twentyonegame.data.objects.Assets
import com.example.twentyonegame.ui.screens.Screens
import com.example.twentyonegame.util.api.TwentyOneServerClient
import com.example.twentyonegame.util.navigation.Navigator
import com.onesignal.OneSignal
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import java.util.TimeZone

class TwentyOneMainViewModel: ViewModel() {
    private var request: Job? = null
    private val server = TwentyOneServerClient.create()

    // Public

    fun init(service: String, id: String) {
        request = viewModelScope.async {
            requestSplash(service, id)
        }
    }

    /// Private

    private suspend fun requestSplash(service: String, id: String) {
        try {
            val time = SimpleDateFormat("z", Locale.getDefault()).format(
                Calendar.getInstance(
                    TimeZone.getTimeZone("GMT"), Locale.getDefault()
                ).time
            )
                .replace("GMT", "")
            val splash = server.getSplash(
                Locale.getDefault().language,
                service,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            viewModelScope.launch {
                if (splash.isSuccessful) {
                    if (splash.body() != null) {
                        when (splash.body()!!.url) {
                            "no" -> switchToApp()
                            "nopush" -> {
                                OneSignal.disablePush(true)
                                switchToApp()
                            }
                            else -> viewModelScope.launch {
                                Assets.cancel()
                                AppData.url = "https://${splash.body()!!.url}"
                                Navigator.navigateTo(Screens.WEB_VIEW)
                            }
                        }
                    } else switchToApp()
                } else switchToApp()
            }
        } catch (e: Exception) {
            switchToApp()
        }
    }

    private fun switchToApp() {
        Assets.assetsState.onEach {
            when(it) {
                AssetsState.ERROR -> Navigator.navigateTo(Screens.NETWORK_ERROR_SCREEN)
                AssetsState.LOADED -> {
                    Navigator.navigateTo(Screens.MENU_SCREEN)
                }
                else -> {}
            }
        }.launchIn(MainScope())
    }
}