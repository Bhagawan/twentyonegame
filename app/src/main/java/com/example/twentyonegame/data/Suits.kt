package com.example.twentyonegame.data

enum class Suits {
    CLUB, HEART, SPADE, DIAMOND
}