package com.example.twentyonegame.data

import androidx.annotation.Keep

@Keep
data class TwentyOneSplashResponse(val url : String)