package com.example.twentyonegame.data

class Card(val suit: Suits, val name: CardNames) {
    val fullName = name.name + "_OF_" + suit.name + "S"
}