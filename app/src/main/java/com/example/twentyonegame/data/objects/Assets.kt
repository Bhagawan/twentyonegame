package com.example.twentyonegame.data.objects

import android.content.Context
import android.graphics.Bitmap
import androidx.core.graphics.drawable.toBitmap
import coil.ImageLoader
import coil.decode.SvgDecoder
import coil.request.ErrorResult
import coil.request.ImageRequest
import coil.request.SuccessResult
import com.example.twentyonegame.data.AssetsState
import com.example.twentyonegame.data.Deck
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlin.coroutines.EmptyCoroutineContext

object Assets {
    private val downloadScope = CoroutineScope(EmptyCoroutineContext)
    private var loader: ImageLoader? = null

    val deckBitmaps = mutableMapOf<String, Bitmap>()
    var cardBackBitmap: Bitmap? = null

    private val _assetsState = MutableStateFlow(AssetsState.LOADING)
    val assetsState = _assetsState.asStateFlow()

    fun loadAssets(context: Context) {
        loader = ImageLoader.Builder(context)
            .components {
                add(SvgDecoder.Factory(false))
            }.build()

        val deck = Deck()
        for(card in deck.cards) {
            val url = Url_Assets + card.suit.name.lowercase() + "s/" + card.name.name.lowercase() + ".svg"
            downloadBitmap(context, url,
                onSuccess =  {
                    deckBitmaps[card.fullName] = it
                    checkCompletion()
                },
                onError =  {
                    _assetsState.tryEmit(AssetsState.ERROR)
                })
        }

        downloadBitmap(context, UrlCard_back,
            onSuccess =  {
                cardBackBitmap = it
                checkCompletion()
            },
            onError =  {
                _assetsState.tryEmit(AssetsState.ERROR)
            })
    }


    private fun downloadBitmap(context: Context,
                    url: String,
                    onSuccess: (bitmap: Bitmap) -> Unit,
                    onError: (error: Throwable) -> Unit) {
        val loader = loader?: ImageLoader(context)
        val request = ImageRequest.Builder(context)
                .data(url)
                .size(200,400)
                .allowHardware(false)
                .listener(object: ImageRequest.Listener {
                    override fun onError(request: ImageRequest, result: ErrorResult) {
                        onError(result.throwable)
                        super.onError(request, result)
                    }

                    override fun onSuccess(request: ImageRequest, result: SuccessResult) {
                        onSuccess(result.drawable.toBitmap())
                    }
                })
                .build()
        downloadScope.launch {
            loader.execute(request)
        }
    }

    private fun checkCompletion() {
        val deck = Deck()
        var loaded = true
        for(card in deck.cards) {
            if(!deckBitmaps.contains(card.fullName)) {
                loaded = false
                break
            }
        }
        if(cardBackBitmap == null) loaded = false
        if(loaded) _assetsState.tryEmit(AssetsState.LOADED)
    }

    fun cancel() {
        downloadScope.cancel()
    }
}