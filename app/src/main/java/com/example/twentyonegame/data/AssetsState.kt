package com.example.twentyonegame.data

enum class AssetsState {
    LOADING, ERROR, LOADED
}