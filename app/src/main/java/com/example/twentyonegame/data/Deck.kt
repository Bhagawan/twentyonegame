package com.example.twentyonegame.data

class Deck {
    val cards: List<Card> = ArrayList<Card>()
        .apply {
            for(suit in Suits.values()) {
                for(card in CardNames.values()) {
                    add(Card(suit, card))
                }
            }
        }.toList()

    private val cardsInDiscard = ArrayList<String>()

    fun getCard(): Card {
        if(cardsInDiscard.size >= cards.size) cardsInDiscard.clear()
        val card = cards.filter { !cardsInDiscard.contains(it.fullName) }.random()
        cardsInDiscard.add(card.fullName)
        return card
    }

    fun shuffle() {
        cardsInDiscard.clear()
    }
}