package com.example.twentyonegame.data.objects

const val UrlSplash = "TwentyOneGame/splash.php"
const val UrlLogo = "http://195.201.125.8/TwentyOneGame/logo.png"
const val UrlBack = "http://195.201.125.8/TwentyOneGame/back.png"
const val UrlDeck = "http://195.201.125.8/TwentyOneGame/assets/deck.png"
const val UrlCard_back = "http://195.201.125.8/TwentyOneGame/assets/card_back.png"
const val UrlWin = "http://195.201.125.8/TwentyOneGame/assets/win.png"
const val UrlLose = "http://195.201.125.8/TwentyOneGame/assets/lose.png"
const val Url_Assets = "http://195.201.125.8/TwentyOneGame/assets/"