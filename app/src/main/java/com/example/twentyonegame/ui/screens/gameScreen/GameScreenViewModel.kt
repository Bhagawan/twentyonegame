package com.example.twentyonegame.ui.screens.gameScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.twentyonegame.ui.screens.Screens
import com.example.twentyonegame.util.navigation.Navigator
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class GameScreenViewModel: ViewModel() {
    private val _bottomHandScore = MutableStateFlow(0)
    val bottomHandScore = _bottomHandScore.asStateFlow()

    private val _topHandScore = MutableStateFlow(0)
    val topHandScore = _topHandScore.asStateFlow()

    private val _addCardToBottom = MutableSharedFlow<Boolean>()
    val addCardToBottom = _addCardToBottom.asSharedFlow()

    private val _addCardToTop = MutableSharedFlow<Boolean>()
    val addCardToTop = _addCardToTop.asSharedFlow()

    private val _endPopup = MutableStateFlow<Boolean?>(null)
    val endPopup = _endPopup.asStateFlow()

    private val _openCards = MutableSharedFlow<Boolean>()
    val openCards = _openCards.asSharedFlow()

    private var gameActive = true

    fun add() {
        if(gameActive) {
            viewModelScope.launch {
                _addCardToBottom.emit(true)
            }
        }
    }

    fun stop() {
        if(gameActive) {
            end(topHandScore.value != 21 && bottomHandScore.value > topHandScore.value || topHandScore.value > 21)
        }
    }

    fun cardAdded(toBottomHand: Boolean, value: Int) = when(toBottomHand) {
        true -> {
            if(bottomHandScore.value + value == 21) end(true)
            else if(bottomHandScore.value + value > 21) end(false, openHands = false)
            _bottomHandScore.tryEmit(bottomHandScore.value + value)
        }
        false -> {
            if(topHandScore.value + value < 17) viewModelScope.launch {
                _addCardToTop.emit(true)
            }
            _topHandScore.tryEmit(topHandScore.value + value)
        }
    }

    fun updateStartScore(topScore: Int, bottomScore: Int) {
        _topHandScore.tryEmit(topScore)
        _bottomHandScore.tryEmit(bottomScore)
        if(topScore < 17) viewModelScope.launch {
            _addCardToTop.emit(true)
        }
    }

    private fun end(win: Boolean, openHands: Boolean = true) {
        viewModelScope.launch {
            gameActive = false
            _endPopup.emit(win)
            if(openHands) _openCards.emit(true)
            delay(2000)
            Navigator.navigateTo(Screens.MENU_SCREEN)
        }
    }

}