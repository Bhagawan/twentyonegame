package com.example.twentyonegame.ui.views

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import com.example.twentyonegame.data.Card
import com.example.twentyonegame.data.objects.Assets

class Hand(private var x: Float, private var y: Float, private var height: Float, private var maxWidth: Float, private var cardWidth: Float) {
    private var cards = ArrayList<CardWithBack>()

    fun draw(c: Canvas) {
        val p = Paint()
        if(maxWidth < cardWidth * cards.size) {
            val interval = (maxWidth - cardWidth) / (cards.size - 1)
            for (card in cards.withIndex()) {
                val bitmap = (if(card.value.faceUp) Assets.deckBitmaps[card.value.card.fullName]
                else Assets.cardBackBitmap)?: Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888)
                c.drawBitmap(bitmap, null,
                    Rect(
                        (x - maxWidth / 2 + interval * card.index).toInt(),
                        (y - height / 2).toInt(),
                        (x - maxWidth / 2 + cardWidth + interval * card.index).toInt(),
                        (y + height / 2).toInt()
                    ), p)
            }
        } else {
            val startX = x - cards.size / 2.0f * cardWidth
            for (card in cards.withIndex()) {
                val bitmap = (if(card.value.faceUp) Assets.deckBitmaps[card.value.card.fullName]
                else Assets.cardBackBitmap)?: Bitmap.createBitmap(1,1,Bitmap.Config.ARGB_8888)
                c.drawBitmap(bitmap, null,
                    Rect(
                        (startX + cardWidth * card.index).toInt(),
                        (y - height / 2).toInt(),
                        (startX + cardWidth * (card.index + 1)).toInt(),
                        (y + height / 2).toInt()
                    ), p)
            }
        }
    }

    fun addCard(card: Card, faceUp: Boolean = true) {
        cards.add(CardWithBack(card, faceUp))
        cards = ArrayList(cards.sortedBy { it.card.name.value }.groupBy { it.faceUp }.let {
            val l = ArrayList<CardWithBack>()
            it[true]?.let { it1 -> l.addAll(it1.toList()) }
            it[false]?.let { it1 -> l.addAll(it1.toList()) }
            l
        })
    }

    fun clear() {
        cards.clear()
    }

    fun changeParameters(newX: Float = x, newY: Float = y, newH: Float = height, newMaxW: Float = maxWidth, newCardW: Float = cardWidth) {
        x = newX
        y = newY
        height = newH
        maxWidth = newMaxW
        cardWidth = newCardW
    }

    fun openHand() {
        for(card in cards) card.faceUp = true
    }

    fun getTotal(): Int = cards.sumOf { it.card.name.value }

    data class CardWithBack(var card: Card, var faceUp: Boolean)
}
