package com.example.twentyonegame.ui.composables

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun ActionButton(modifier: Modifier = Modifier, text: String, onClick: () -> Unit) {
    Box(modifier = modifier
        .border(width = 3.dp, shape = RoundedCornerShape(10.dp), color = Color.White)
        .clickable( remember { MutableInteractionSource() }, null) { onClick() }, contentAlignment = Alignment.Center) {
        Text(text, fontSize = 20.sp, textAlign = TextAlign.Center, color = Color.White, fontWeight = FontWeight.Bold)
    }
}