package com.example.twentyonegame.ui.views

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.view.MotionEvent
import android.view.View
import com.example.twentyonegame.data.Deck
import com.example.twentyonegame.data.objects.Assets
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer

class TwentyOneGameView(context: Context): View(context){
    private var mWidth = 0
    private var mHeight = 0
    private val deck = Deck()

    private var currentCard = deck.cards.random()
    private val topHand = Hand(0.0f,0.0f, 1.0f, 1.0f,1.0f)
    private val bottomHand = Hand(0.0f,0.0f, 1.0f, 1.0f,1.0f)

    private var mInterface: GameInterface? = null

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            val cardH = mHeight / 5.0f
            val cardWidth = ((Assets.cardBackBitmap?.width?.toFloat()) ?: 10.0f) / ((Assets.cardBackBitmap?.height?.toFloat()) ?: 20.0f) * cardH
            topHand.changeParameters(mWidth / 2.0f, cardH / 2.0f, cardH,  mWidth.toFloat(), cardWidth)
            bottomHand.changeParameters(mWidth / 2.0f, mHeight - cardH / 2.0f, cardH,  mWidth.toFloat(), cardWidth)
        }
    }

    override fun onDraw(canvas: Canvas) {
        drawCards(canvas)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN,
            MotionEvent.ACTION_MOVE -> {
                return true
            }

            MotionEvent.ACTION_UP -> {
                currentCard = deck.cards.random()
                return true
            }
        }
        return false
    }

    //// Public

    fun setInterface(i: GameInterface) {
        mInterface = i
    }

    fun addCard(toBottomHand: Boolean) {
        when(toBottomHand) {
            true -> {
                val card = deck.getCard()
                bottomHand.addCard(card)
                mInterface?.cartAdded(true, card.name.value)
            }
            false -> {
                val card = deck.getCard()
                topHand.addCard(card)
                mInterface?.cartAdded(false, card.name.value)
            }
        }
    }

    fun startGame() {
        var card = deck.getCard()
        bottomHand.addCard(card)

        card = deck.getCard()
        bottomHand.addCard(card)

        card = deck.getCard()
        topHand.addCard(card)

        card = deck.getCard()
        topHand.addCard(card, false)
        mInterface?.updateStartScore(topHand.getTotal(), bottomHand.getTotal())
    }

    fun openCards() {
        topHand.openHand()
    }

    //// Private

    private fun drawCards(c: Canvas) {
        topHand.draw(c)
        bottomHand.draw(c)
    }

    interface GameInterface {
        fun cartAdded(bottomHand: Boolean, value: Int)
        fun updateStartScore(topScore: Int, bottomScore: Int)
    }
}