package com.example.twentyonegame.ui.composables

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import coil.compose.rememberAsyncImagePainter
import com.example.twentyonegame.data.objects.UrlLose
import com.example.twentyonegame.data.objects.UrlWin
import com.example.twentyonegame.ui.theme.Transparent_black

@Composable
fun EndPopup(win: Boolean) {
    val url = if(win) UrlWin else UrlLose
    AnimatedVisibility(true,
        enter = fadeIn(tween(500))
    ) {
        BoxWithConstraints(modifier = Modifier
            .fillMaxSize()
            .background(Transparent_black), contentAlignment = Alignment.Center) {
            val w = maxWidth * 0.7f
            Image(rememberAsyncImagePainter(url), contentDescription = null, modifier = Modifier.size(w, w * 0.5f), contentScale = ContentScale.FillWidth)
        }
    }
}