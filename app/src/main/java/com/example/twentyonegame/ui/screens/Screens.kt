package com.example.twentyonegame.ui.screens

enum class Screens(val label: String) {
    SPLASH_SCREEN("splash"),
    NETWORK_ERROR_SCREEN("network_error"),
    WEB_VIEW("web_view"),
    MENU_SCREEN("menu"),
    GAME_SCREEN("game")
}