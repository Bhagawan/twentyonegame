package com.example.twentyonegame.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import com.example.twentyonegame.R
import com.example.twentyonegame.data.objects.UrlBack
import com.example.twentyonegame.util.navigation.Navigator

@Preview
@Composable
fun MenuScreen() {
    Image(rememberAsyncImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    BoxWithConstraints(modifier = Modifier
        .fillMaxSize()
        .clickable(remember { MutableInteractionSource() }, null) { Navigator.navigateTo(Screens.GAME_SCREEN) },
        contentAlignment = Alignment.Center) {
        val brush = Brush.linearGradient(
            0.0f to Color(255, 228, 1, 255),
            0.1f to Color(255, 247, 194, 255),
            0.3f to Color(255, 228, 1, 255),
            0.9f to Color(255, 138, 0, 255),
            1.0f to Color(173, 104, 0, 255),
            start = Offset(maxWidth.value / 2.0f, 0.0f),
            end = Offset(maxWidth.value / 2.0f, maxHeight.value)
        )
        Text(stringResource(id = R.string.btn_start), textAlign = TextAlign.Center, fontWeight =  FontWeight.Bold, fontSize = 40.sp, color = Color.Black,
            modifier = Modifier.background(brush, RoundedCornerShape(10.dp)).padding(vertical = 10.dp, horizontal = 50.dp))
    }
}