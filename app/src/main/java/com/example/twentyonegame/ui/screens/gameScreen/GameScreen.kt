package com.example.twentyonegame.ui.screens.gameScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.example.twentyonegame.R
import com.example.twentyonegame.data.objects.UrlDeck
import com.example.twentyonegame.ui.composables.ActionButton
import com.example.twentyonegame.ui.composables.EndPopup
import com.example.twentyonegame.ui.views.TwentyOneGameView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun GameScreen() {
    var init  by rememberSaveable { mutableStateOf(true) }
    val viewModel = viewModel(GameScreenViewModel::class.java)

    val bottomHandScore by viewModel.bottomHandScore.collectAsState()
    val endPopup by viewModel.endPopup.collectAsState()

    Image(rememberAsyncImagePainter(UrlDeck), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally) {
        AndroidView(factory = { TwentyOneGameView(it) },
            modifier = Modifier
                .weight(4.0f, true)
                .fillMaxWidth(),
            update = { view ->
                if (init) {
                    view.setInterface(object : TwentyOneGameView.GameInterface {
                        override fun cartAdded(bottomHand: Boolean, value: Int) {
                            viewModel.cardAdded(bottomHand, value)
                        }

                        override fun updateStartScore(topScore: Int, bottomScore: Int) {
                            viewModel.updateStartScore(topScore, bottomScore)
                        }
                    })

                    viewModel.addCardToTop.onEach { view.addCard(false) }.launchIn(viewModel.viewModelScope)
                    viewModel.addCardToBottom.onEach { if(it) view.addCard(true) }.launchIn(viewModel.viewModelScope)
                    viewModel.openCards.onEach { if(it) view.openCards() }.launchIn(viewModel.viewModelScope)
                    init = false

                    view.startGame()
                }
            })
        Text(stringResource(id = R.string.score) + bottomHandScore.toString(),
            fontWeight = FontWeight.Bold,
            fontSize = 14.sp,
            textAlign = TextAlign.Center,
            color = Color.White)
        Row(modifier = Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically, ) {
            ActionButton(modifier = Modifier
                .weight(1.0f, true)
                .padding(vertical = 5.dp, horizontal = 10.dp), text = stringResource(id = R.string.action_stop)) {
                viewModel.stop()
            }
            ActionButton(modifier = Modifier
                .weight(1.0f, true)
                .padding(vertical = 5.dp, horizontal = 10.dp), text = stringResource(id = R.string.action_add)) {
                viewModel.add()
            }
        }
    }
    endPopup?.let { EndPopup(win = it) }

}